interface Listener<TEvent> {
    (event: TEvent): void;
}

export function hook<TEvent>() {

    let listeners: Array<Listener<TEvent>> = [];
    let busy = false;

    function hook(event: TEvent)
    function hook(listener: Listener<TEvent>)
    function hook(arg) {
        if (typeof arg === "function") {
            listeners.push(arg);
        } else {
            if (busy) {
                return;
            }
            
            busy = true;

            for (let listener of listeners) {
                listener(arg);
            }

            busy = false;
        }
    }

    return hook;
}
