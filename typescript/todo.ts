import { hook } from "./events";
import * as deepstream from "deepstream.io-client-js";

class Model {
    private name: string;

    setName(name: string) {
        this.name = name;

        this.onSetName({ name });
    }

    onSetName = hook<{ name: string }>();
}

class View {
    private model: Model;
    private root: HTMLElement;

    constructor(model: Model, root: HTMLElement) {
        this.model = model;
        this.root = root;

        root.innerHTML = `
            Name: <input type="text" class="name" value="" placeholder="Enter your name">
            <h2 data-id="greeting"></h2>
        `;

        model.onSetName(e => this.showName(e.name));

        model.onSetName(e => this.showGreeting(e.name));

        this.getNameInput().addEventListener("keyup", e => {
            this.model.setName(this.getNameInput().value);
        });
    }

    private getNameInput() {
        return this.root.querySelector('input.name') as HTMLInputElement;
    }

    private showGreeting(name: string) {
        let h2 = this.root.querySelector('[data-id="greeting"]');

        h2.textContent = `Hello, ${name}!!`;
    }

    private showName(name: string) {
        let input = this.getNameInput();

        if (name != input.value) {
            input.value = name;
        }
    }
}

class Storage {
    constructor(model: Model, io: deepstreamIO.deepstreamQuarantine, record_id: string) {
        var record = io.record.getRecord(record_id);

        record.subscribe("name", value => model.setName(value));

        model.onSetName(e => record.set("name", e.name));
    }
}

export function init(root: HTMLElement) {
    var model = new Model();

    var storage = new Storage(model, deepstream('localhost:6020').login(), "hello");

    var view = new View(model, root);

    model.setName("Rasmus");

    console.log(model);
}
