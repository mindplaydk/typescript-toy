define("events", ["require", "exports"], function (require, exports) {
    "use strict";
    function hook() {
        var listeners = [];
        var busy = false;
        function hook(arg) {
            if (typeof arg === "function") {
                listeners.push(arg);
            }
            else {
                if (busy) {
                    return;
                }
                busy = true;
                for (var _i = 0, listeners_1 = listeners; _i < listeners_1.length; _i++) {
                    var listener = listeners_1[_i];
                    listener(arg);
                }
                busy = false;
            }
        }
        return hook;
    }
    exports.hook = hook;
});
define("todo", ["require", "exports", "events", "deepstream.io-client-js"], function (require, exports, events_1, deepstream) {
    "use strict";
    var Model = (function () {
        function Model() {
            this.onSetName = events_1.hook();
        }
        Model.prototype.setName = function (name) {
            this.name = name;
            this.onSetName({ name: name });
        };
        return Model;
    }());
    var View = (function () {
        function View(model, root) {
            var _this = this;
            this.model = model;
            this.root = root;
            root.innerHTML = "\n            Name: <input type=\"text\" class=\"name\" value=\"\" placeholder=\"Enter your name\">\n            <h2 data-id=\"greeting\"></h2>\n        ";
            model.onSetName(function (e) { return _this.showName(e.name); });
            model.onSetName(function (e) { return _this.showGreeting(e.name); });
            this.getNameInput().addEventListener("keyup", function (e) {
                _this.model.setName(_this.getNameInput().value);
            });
        }
        View.prototype.getNameInput = function () {
            return this.root.querySelector('input.name');
        };
        View.prototype.showGreeting = function (name) {
            var h2 = this.root.querySelector('[data-id="greeting"]');
            h2.textContent = "Hello, " + name + "!!";
        };
        View.prototype.showName = function (name) {
            var input = this.getNameInput();
            if (name != input.value) {
                input.value = name;
            }
        };
        return View;
    }());
    var Storage = (function () {
        function Storage(model, io, record_id) {
            var record = io.record.getRecord(record_id);
            record.subscribe("name", function (value) { return model.setName(value); });
            model.onSetName(function (e) { return record.set("name", e.name); });
        }
        return Storage;
    }());
    function init(root) {
        var model = new Model();
        var storage = new Storage(model, deepstream('localhost:6020').login(), "hello");
        var view = new View(model, root);
        model.setName("Rasmus");
        console.log(model);
    }
    exports.init = init;
});
//# sourceMappingURL=todo.js.map